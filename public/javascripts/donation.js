$('.ui.dropdown').dropdown();
$('.ui.form').form({
	lawndaleStudent : {
		identifier : 'lawndaleStudent',
		rules : [ {
			type : 'checked',
			prompt : 'Verify you go to Lawndale'
		} ]
	},
	firstName : {
		identifier : 'firstName',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a first name'
		} ]
	},
	lastName : {
		identifier : 'lastName',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a last name'
		} ]
	},
	clique : {
		identifier : 'clique',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter your social clique'
		} ]
	},
	office : {
		identifier : 'office',
		rules : [ {
			type : 'empty',
			prompt : 'Please select the office this candidate is running for'
		} ]
	},
	email : {
		identifier : 'email',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a valid email'
		} ]
	},
	password : {
		identifier : 'password',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a password'
		}, {
			type : 'length[6]',
			prompt : 'Your password must be at least 6 characters'
		} ]
	},
	schoolStatus : {
		identifier : 'schoolStatus',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter your status'
		} ]
	},
	conditions : {
		identifier : 'conditions',
		rules : [ {
			type : 'checked',
			prompt : 'You must agree to the terms and conditions'
		} ]
	},
	phone : {
		identifier : 'phone',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a phone number'
		}]
	},
	hq : {
		identifier : 'hq',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter the headquarters for this candidate'
		} ]
	},
	campaignMessage : {
		identifier : 'campaignMessage',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a campaign message for this candidate'
		} ]
	},
	bio : {
		identifier : 'bio',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a bio for this candidate'
		} ]
	},
	target : {
		identifier : 'target',
		rules : [ {
			type : 'empty',
			prompt : 'Please enter a target number of pizza slices'
		}]
	},
	id : {
		identifier : 'id',
		rules : [ {
			type : 'empty',
			prompt : 'Please select a candidate'
		} ]
	},
	received : {
		identifier : 'received',
		rules : [ {
			type : 'empty',
			prompt : 'Please select a number of slices'
		} ]
	},
	type : {
		identifier : 'type',
		rules : [ {
			type : 'empty',
			prompt : 'Please select a type of pizza'
		} ]
	}
});


