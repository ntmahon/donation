package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import play.db.jpa.Model;

@Entity
public class Donation extends Model 
{
    public long received;
    public String type;
    
    @ManyToOne
    public Candidate to;
    
    @ManyToOne
    public User from;

    public Donation(Candidate to, User from, long received, String type) 
    {
    	this.to = to;
        this.received = received;
        this.type = type;
        this.from = from;
    }
}