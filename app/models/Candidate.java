  package models;

  import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;
import play.db.jpa.Blob;

  @Entity
  public class Candidate extends Model
  {
    public String firstName;
    public String lastName;
    public String email;
    public String phone;
    public String office;
    public String hq;
    public String campaignMessage;
    public String bio;
    public int target;
    public Blob picture;
    
    @OneToMany(mappedBy="to", cascade=CascadeType.ALL)
    public List<Donation> donations;

    public Candidate(String firstName, 
              String lastName,
              String email, 
              String phone,
              String office,
              String hq,
              String campaignMessage,
              String bio,
              int target)
    {
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
      this.phone = phone;
      this.office = office;
      this.hq = hq;
      this.campaignMessage = campaignMessage;
      this.bio = bio;
      this.target = target;
    }
    
    public void addDonation(Donation donation) {
    	   donations.add(donation);
    	  }

    public static Candidate findByEmail(String email) 
    {
        return find("email", email).first();
    }

  }