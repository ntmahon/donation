  package models;

  import play.db.jpa.Model;
  import javax.persistence.Entity;

  @Entity
  public class Admin extends Model
  {
    public String userName;
    public String password;

    public Admin(String userName, String password)
    {
      this.userName = userName;
      this.password = password;

    }

    public static Admin findByName(String userName) 
    {
        return find("userName", userName).first();
    }

    public boolean checkPassword(String password) 
    {
        return this.password.equals(password);
    }
  }