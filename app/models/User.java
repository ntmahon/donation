  package models;

  import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

  @Entity
  public class User extends Model
  {
    public boolean lawndaleStudent;
    public String firstName;
    public String lastName;
    public String clique;
    public String email;
    public String password;
    public String schoolStatus;
    public String address1;
    public String address2;
    public String city;
    public String postcode;
    public String country;
    
    @OneToMany(mappedBy="from", cascade=CascadeType.ALL)
    public List<Donation> donations;

    public User(boolean lawndaleStudent,
              String firstName, 
              String lastName,
              String clique,
              String email, 
              String password,
              String schoolStatus,
              String address1,
              String address2,
              String city,
              String postcode,
              String country)
    {
      this.lawndaleStudent = lawndaleStudent;
      this.firstName = firstName;
      this.lastName = lastName;
      this.clique = clique;
      this.email = email;
      this.password = password;
      this.schoolStatus = schoolStatus;
      this.address1 = address1;
      this.address2 = address2;
      this.city = city;
      this.postcode = postcode;
      this.country = country;
    }

    public static User findByEmail(String email) 
    {
        return find("email", email).first();
    }

    public boolean checkPassword(String password) 
    {
        return this.password.equals(password);
    }
  }