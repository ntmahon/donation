
import java.io.FileNotFoundException;
import java.util.List;

import play.db.DB;
import play.*;
import play.jobs.*;
import play.test.*;
import models.*;

@OnApplicationStart
public class Bootstrap extends Job 
{ 
  public void doJob() throws FileNotFoundException
  {
	  Fixtures.deleteDatabase();
	  Fixtures.loadModels("data.yml");
	  
	  List<Candidate> candidates = Candidate.findAll();
	  
	  for(Candidate candidate : candidates)
	  {
		  String command = "update candidate set picture='" +  candidate.firstName.toLowerCase() + ".jpg|image/jpeg' where id=" + candidate.id;
		  DB.execute(command);
	  }
  }
}