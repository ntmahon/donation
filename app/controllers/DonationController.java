package controllers;

import play.*;
import play.db.jpa.JPA;
import play.mvc.*;

import java.util.*;

import javax.persistence.Query;

import models.*;

public class DonationController extends Controller 
{
	public static void index()
	{
		User user = Accounts.getCurrentUser();
        if (user == null) 
        {
            Logger.info("Unable to get current user");
            Accounts.login();
        } 
        else
        {
        	List<Candidate> candidates = Candidate.findAll();
        	render(candidates);
        }
	}

    public static void makeDonation(Long id) 
    {
        User user = Accounts.getCurrentUser();
        if (user == null) 
        {
            Logger.info("Unable to get current user");
            Accounts.login();
        } 
        else 
        {
        	List<Candidate> candidates = Candidate.findAll();      
            render(candidates, user);
        }
    }
    
    public static void learnMore(Long id)
    {
    	 User user = Accounts.getCurrentUser();
         if (user == null) 
         {
             Logger.info("Unable to get current user");
             Accounts.login();
         } 
         else
         {
        	 Candidate candidate = Candidate.findById(id);
         	 String prog = getPercentTargetAchieved(id);
             String progress = prog + "%";
             Logger.info("visiting candidate " + candidate.firstName);
             Logger.info("percent target achieved " + progress);
             render(candidate, progress);
         }
    }
    /**
     * Log and save to database amount donated and method of donation, eg.
     * paypal, direct payment
     * 
     * @param amountDonated
     * @param type
     * @param to 
     */
    public static void donate(long id, long received, String type) 
    {
        Logger.info("amount donated " + received + " " + "type "
                + type);

        User user = Accounts.getCurrentUser();
        if (user == null) 
        {
            Logger.info("Donation class : Unable to getCurrentuser");
            Accounts.login();
        } 
        else 
        {
            addDonation(id, user, received, type);
        }
        learnMore(id);
    }

    /**
     * @param user
     * @param amountDonated
     */
    private static void addDonation(Long id, User from, long received, String type) 
    {
    	Candidate to = Candidate.findById(id);
        Donation donate = new Donation(to, from, received, type);
        donate.save();
        to.donations.add(donate);
    }

    public static String getPercentTargetAchieved(long id) 
    {
    	Candidate candidate = Candidate.findById(id);
        List<Donation> allDonations = Donation.findAll();
        long total = 0;
        for (Donation donation : allDonations) 
        {
        	if (donation.to == candidate)
            total += donation.received;
        }
        long target = candidate.target;
        long percentachieved = (total * 100 / target);
        String progress = String.valueOf(percentachieved);
        Logger.info("Percent of target achieved (string) " + progress
                + " percentachieved (long)= " + percentachieved);
        return progress;
    }
    
    public static void renderReport()
    {
      List<Donation> donations = Donation.findAll();
      render(donations);
    }
}