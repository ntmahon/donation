package controllers;

import play.*;
import play.mvc.*;
import play.db.jpa.Blob;
import java.util.*;
import models.*;

public class Edit extends Controller {

	public static void index() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			Accounts.login();
		}
		User user = User.findById(Long.parseLong(userId));
		render(user);
	}

	public static void changeName(String firstName, String lastName) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.firstName = firstName;
		user.lastName = lastName;
		user.save();
		Logger.info("Name changed to: " + firstName + " " + lastName);
		index();
	}
	
	public static void changeAddress(String address1, String address2, String city, String postcode, String country) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.address1 =address1;
		user.address2 =address2;
		user.city =city;
		user.postcode =postcode;
		user.country =country;
		user.save();
		Logger.info("Address changed to: " + address1 + ", " + address2 + ", " + city + ", " + postcode + ", " + country);
		index();
	}
	
	public static void changeClique(String clique) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.clique = clique;
		user.save();
		Logger.info("Clique changed to: " + clique);
		index();
	}

	public static void changeStatus(String schoolStatus) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.schoolStatus = schoolStatus;
		user.save();
		Logger.info("Status changed to: " + schoolStatus);
		index();
	}
	
	public static void changeEmail(String email, String password) {
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		user.email = email;
		user.password = password;
		user.save();
		Logger.info("Email and password changed to: " + email + " " + password);
		index();
	}
}