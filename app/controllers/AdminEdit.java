package controllers;

import play.*;
import play.mvc.*;
import play.db.jpa.Blob;

import java.util.*;

import models.*;

public class AdminEdit extends Controller 
{

    public static void index()
    {
    	Admin admin = AdminController.getCurrentUser();
        if (admin == null) 
        {
            Logger.info("Unable to get current user");
            Welcome.index();
        } 
        else
        {
        	List<Candidate> candidates = Candidate.findAll();
        	render(candidates);
        }
    }
    
    public static void editPage(Long id)
    {
    	Admin admin = AdminController.getCurrentUser();
        if (admin == null) 
        {
            Logger.info("Unable to get current user");
            Welcome.index();
        } 
        else
        {
        	Candidate candidate = Candidate.findById(id);
    		render(candidate);
        }
    } 
    
    public static void changeName(Long id, String firstName, String lastName) {
		Candidate candidate = Candidate.findById(id);
		candidate.firstName = firstName;
		candidate.lastName = lastName;
		candidate.save();
		Logger.info("Name changed to: " + firstName + " " + lastName);
		editPage(id);
	}
    
    public static void changeOffice(Long id, String office) {
		Candidate candidate = Candidate.findById(id);
		candidate.office = office;
		candidate.save();
		Logger.info("Office changed to: " + office);
		editPage(id);
	}
    
    public static void changeEmail(Long id, String email) {
		Candidate candidate = Candidate.findById(id);
		candidate.email = email;
		candidate.save();
		Logger.info("Email changed to: " + email);
		editPage(id);
	}
    
    public static void changePhone(Long id, String phone) {
		Candidate candidate = Candidate.findById(id);
		candidate.phone = phone;
		candidate.save();
		Logger.info("Phone changed to: " + phone);
		editPage(id);
	}
    
    public static void changeHq(Long id, String hq) {
		Candidate candidate = Candidate.findById(id);
		candidate.hq = hq;
		candidate.save();
		Logger.info("HQ changed to: " + hq);
		editPage(id);
	}
    
    public static void changeCampaignMessage(Long id, String campaignMessage) {
		Candidate candidate = Candidate.findById(id);
		candidate.campaignMessage = campaignMessage;
		candidate.save();
		Logger.info("Campaign Message changed to: " + campaignMessage);
		editPage(id);
	}
    
    public static void changeBio(Long id, String bio) {
		Candidate candidate = Candidate.findById(id);
		candidate.bio = bio;
		candidate.save();
		Logger.info("Bio changed to: " + bio);
		editPage(id);
	}
    
    public static void changeTarget(Long id, int target) {
		Candidate candidate = Candidate.findById(id);
		candidate.target = target;
		candidate.save();
		Logger.info("Target changed to: " + target);
		editPage(id);
	}
    
	public static void getPicture(Long id) {
		Candidate candidate = Candidate.findById(id);
		Blob picture = candidate.picture;
		if (picture.exists()) {
			response.setContentTypeIfNotSet(picture.type());
			renderBinary(picture.get());
		}
	}
    
    public static void uploadPicture(Long id, Blob picture) {
    	Candidate candidate = Candidate.findById(id);
		candidate.picture = picture;
		candidate.save();
		Logger.info("Piture changed");
		editPage(id);
	}
}