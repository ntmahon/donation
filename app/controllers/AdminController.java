package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class AdminController extends Controller 
{
    public static void login() 
    {
        render();
    }
    
    public static void home()
    {
    	Admin admin = getCurrentUser();
        if (admin == null) 
        {
            Logger.info("Unable to get current user");
            Welcome.index();
        } 
        else
        {
        	List<Candidate> candidates = Candidate.findAll();
        	List<User> users = User.findAll();
        	render(candidates, users);
        }
    }
    
    public static void addPage()
    {
    	Admin admin = getCurrentUser();
        if (admin == null) 
        {
            Logger.info("Unable to get current user");
            Welcome.index();
        } 
        else
        {
        	render();
        }
    } 
	
    public static void authenticate(String userName, String password) 
    {
        Logger.info("Attempting to authenticate with " + userName + ":" + password);

        Admin admin = Admin.findByName(userName);
        if ((admin != null) && (admin.checkPassword(password) == true)) 
        {
            Logger.info("Successfull authentication of  " + admin.userName);
            session.put("logged_in_adminid", admin.id);
            home();
        } 
        else 
        {
            Logger.info("Authentication failed");
            login();
        }
    }
    
    public static void addCandidate(String firstName,
            String lastName, String email, String phone, String office,
            String hq, String campaignMessage, String bio, int target) 
    {
        Logger.info(firstName + " " + lastName + " " + email + " " + office);

        Candidate candidate = new Candidate(firstName, lastName, email, phone, office, hq, campaignMessage, bio, target);

        candidate.save();

        home();
    }

    public static Admin getCurrentUser() 
    {
        String userId = session.get("logged_in_adminid");
        if(userId == null)
        {
            return null;
        }
        Admin logged_in_admin = Admin.findById(Long.parseLong(userId));
        Logger.info("Logged in admin is " + logged_in_admin.userName);
        return logged_in_admin;
    }
    

}