Donation is a java app built in the Play Framework, version 1.2.7

This app was built as a college project.

The purpose of the app is to allow users to create accounts, log in and donate to political candidates.

There is also an administrator role which allows an admin user log in and add/edit candidates, and view details on donations made.